#To run you app you need:
- cp .env.example .env
- docker-compose up -d
- docker-compose exec php composer install
- docker-compose exec php composer drupal-install-clean
