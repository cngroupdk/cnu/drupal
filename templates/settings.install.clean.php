<?php

/**
 * @file
 * Configuration which is stable for site-install.
 */

$settings['hash_salt'] = 'T_Erq4yEJzLln9jvadJTbKcpIYyG4-R_9o0Tu6NJLyA';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['file_private_path'] = '../files-private';

$databases = [];
$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'username' => getenv('MYSQL_USER'),
  'password' => getenv('MYSQL_PASSWORD'),
  'prefix' => '',
  'host' => 'mysql',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

$settings['config_sync_directory'] =  '../config/clean';
