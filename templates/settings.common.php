<?php

/**
 * @file
 * Configuration related to fully installed site.
 */

$settings['cache']['default'] = 'cache.backend.memcache';
$settings['cache']['bins']['bootstrap']    = 'cache.backend.chainedfast';
$settings['cache']['bins']['config']       = 'cache.backend.chainedfast';
$settings['cache']['bins']['container']    = 'cache.backend.chainedfast';
$settings['cache']['bins']['discovery']    = 'cache.backend.chainedfast';

$settings['container_yamls'][] = DRUPAL_ROOT . 'sites/default/services.local.yml';

if (file_exists(DRUPAL_ROOT . '/sites/default/settings.local.php')) {
    include DRUPAL_ROOT . '/sites/default/settings.local.php';
}
