<?php

/**
 * @file
 * Drupal local environment configuration file.
 */

$config['system.logging']['error_level'] = 'verbose';

/**
 * Disable CSS and JS aggregation.
 */
//$config['system.performance']['css']['preprocess'] = FALSE;
//$config['system.performance']['js']['preprocess'] = FALSE;

$settings['memcache']['servers'] = [
  'memcached:11211' => 'default'
];
$settings['memcache']['key_prefix'] = 'cnu';
