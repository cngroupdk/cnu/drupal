<?php

namespace Drupal\cnu_demo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for CNU demo routes.
 */
class CnuDemoDefaultController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('This is CNU index page!'),
    ];

    return $build;
  }

}
